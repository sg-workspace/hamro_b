
<?php
	define("DEFAULT_TIMEZONE", "Asia/Kathmandu");
	function mainRequestValue()
  	{
   	 if(isset($_POST['request'])){
   	   return $_POST['request'];
   	 }
   	 elseif (isset($_GET['request'])) {
   	   return $_GET['request'];
   	 }
   	 else {
   	   return "unset";
    	}
 	 }

	$req = mainRequestValue();


	if($req=='unset'){
		include('./front/index.php');
	}
	elseif($req=='register_page'){
		include('./front/register.php');

	}
	elseif($req=='home_page'){
		include('./front/index.php');

	}
	elseif($req=='faq_page'){
		include('./front/faq.php');

	}
	elseif($req=='sell_page'){
		include('./front/sell.php');

	}
	elseif($req == 'register_new_user'){
		include './back/logics/userTasks.php';
		include './back/models/user.php';
		include './back/logics/hashing.php';
		include './back/logics/dateTime.php';
		$hasher = new Hashing(trim($_POST['user_password']));
		$newUser = new User($_POST['user_first_name'], $_POST['user_last_name'], $_POST['user_email'], $hasher->getPwdHash(), $_POST['user_address'], $_POST['user_contact_number']);
		$cDT = new CustomDateTime();
		$newUser->setUserId("uid-".$cDT->getDateTimeStamp());
		$ut = new UserTasks();
		if($ut->registerNewUser($newUser)){
			echo "Register Success";
		}
		else{
			echo "Register Fail";
		}

	}
	elseif ($req == 'user_login') {
		include './back/logics/authentication.php';
		include './back/logics/userTasks.php';
		$userTasks = new UserTasks();
		$userDetails = $userTasks->getUserDetails($_POST['user_email']);
		//echo $userDetails->getUserId();
		$auth = new Authenticator($userDetails,$_SERVER['REQUEST_TIME']);
		if ($auth->pwdMatchCheck(trim($_POST['user_password'])) == "match")
		{
			$user_id = $userDetails->getUserId();
			$auth_key = $auth->generateAuthKey();
		    echo "{\"user_id\" : \"$user_id\", \"auth_key\" : \"$auth_key\"}";
		}
		else
		{
			echo "error";
		}
	}	
	else
	{

	}
?>
