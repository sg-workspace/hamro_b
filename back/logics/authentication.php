<?php
  include './back/logics/hashing.php';
  class Authenticator
  {
    //private $userId;//userID means Email here in this case
    private $reqTime; //the $_SERVER['REQUEST_TIME'] at login backend
    private $authKey; 
    private $userDetails;
     //key generated upon successful login and to be stored
    //both in session and front end cache for authentication
    function __construct($userDetails, $reqTime)
    {
      $this->userDetails = $userDetails;
      $this->reqTime = $reqTime;
    }
    public function pwdMatchCheck($sentPwd){
     //include './back/db/userDb.php';
     //$db = new UserDb();
      $authMsg;
      //get hashed pwd from db;
      $hashedPwdFromDb = $this->userDetails->getPassword();
      //hash the user provided pwd
      $hashing = new Hashing($sentPwd);
      $hashedPwdSent = $hashing->getPwdHash();
      //echo "from db:\n". $hashedPwdFromDb . "\nsent hashed:\n" . $hashedPwdSent;
      //compare the $hashes
      if(trim($hashedPwdSent)==trim($hashedPwdFromDb)){
        $authMsg = "match";
      }
      else{
        $authMsg = $hashedPwdSent;
      }
      return $authMsg;
    }
    public function generateAuthKey(){
      //generate logged-in-hash
      $hashing = new Hashing($this->reqTime);
      $this->authKey = $hashing->generateAuthKey();
      session_start();
      //store the $authKey, login-time (ie $reqTime to )
      $_SESSION[$this->userDetails->getUserId()] = ["auth_key"=>$this->authKey, "last_req_time"=>$this->reqTime];
      return $this->authKey;
    }
    //the getters
    public function getUserId()
    {
      return $this->userId;
    }
    public function getReqTime()
    {
      return $this->reqTime;
    }
    public function getAuthKey()
    {
      return $this->authKey;
    }
  }
?>
