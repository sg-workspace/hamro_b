<?php
  class CustomDateTime
  {
    private $dateTimeObj;
    function __construct()
    {
      $dateTime = new DateTime();
      $dateTime->setTimeZone(new DateTimeZone(DEFAULT_TIMEZONE));
      $this->dateTimeObj = $dateTime;
    }
    public function getDateTimeStamp(){
      return $this->dateTimeObj->getTimestamp();
    }
  }

?>
