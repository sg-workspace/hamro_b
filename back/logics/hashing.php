<?php
  class Hashing
  {
    private $stringToHash;
    private $hash;
    function __construct($stringToHash)
    {
      $this->stringToHash = $stringToHash;
    }

    //for now pwd be hashed with sha256 algorithm with false(non-raw ie hex) (64bytes result size)
    function getPwdHash(){
      $this->hash = hash('sha256', $this->stringToHash, FALSE);
      return $this->hash;
    }

    /**login key hash can be done with weaker algorithm such as md5
    this will be shared between server and client for confirming login and data
    is coming from the single and latest currently logged-in device. This also
    ensures only a single front end usage at a time and automatic logout in case
    of login from another device */
    function generateAuthKey(){
      $this->hash = hash('md5', $this->stringToHash, FALSE);
      return $this->hash;
    }
  }

?>
