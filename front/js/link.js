const signUpButton = document.getElementById('signUp');
const signInButton = document.getElementById('signIn');
const container = document.getElementById('container');

signUpButton.addEventListener('click', () => {
	container.classList.add("right-panel-active");
});

signInButton.addEventListener('click', () => {
	container.classList.remove("right-panel-active");
});



function buy(){
	$('#how-to-sell-text').hide();
	$('#safety-tips-text').hide();
	$('#how-to-buy-text').show();

}

function sell(){
	$('#how-to-buy-text').hide();
	$('#safety-tips-text').hide();
	$('#how-to-sell-text').show();
}

function safety(){
	$('#how-to-buy-text').hide();
	$('#how-to-sell-text').hide();
	$('#safety-tips-text').show();
}

function userRegister(){
	var first_name = $("#user_first_name").val();
	var last_name = $("#user_last_name").val();
	var email = $("#user_email").val();
	var password = $("#user_password").val();
	var confirm_password = $("#user_confirm_password").val();
	var contact_number = $("#user_contact").val();
	var address = $("#user_address").val();
	var regEx = /\S+@\S+\.\S+/;

	if (!first_name || !last_name || !email || !password || !confirm_password || !contact_number || !address) 
	    {
	     alert("All Fields are Required.")
	    }
	    if(password!=confirm_password)
	    {
	    	alert("Password Does not match")
	    }
 
	    var validEmail = regEx.test(email);
	     if (!validEmail)
	      {
	      	alert("Pleae Enter Valid Email");
	     }
	     else{
		     	$.ajax({
							method: "POST",
							url: "",
							data: { user_first_name: first_name, user_last_name: last_name, user_email: email,
								  user_password: password, user_contact_number:contact_number, user_address:address,
								  request:'register_new_user'}
					})
						.done(function( data, textStatus, jqXHR) {
							console.log("ajax_status: " + textStatus + " resonse-data.lenth: " + data.trim().length);
							if(textStatus === "success"){
								alert((data).trim());
							}
							else{
								alert(data);
							}
						});
	     }
}

let authKey;
function userLogin(){
	var email = $("#user_signin_email").val();
	var password = $("#user_signin_password").val();
	var regEx = /\S+@\S+\.\S+/;
	console.log(email);
	var validEmail = regEx.test(email);
	 if (!validEmail){
	    	alert("Please Enter Valid Email");
	 }

	 else {   	
			if (email=="" || password=="")
   				 {
					alert("Please enter your email and password");				
				 }
	
				else{
						$.ajax({
							method: "POST",
							url: "",
							data: { user_email: email, user_password: password,request:'user_login' }
						})
						.done(function( data, textStatus, jqXHR ) {
							if(textStatus =="success")
							{	
							      if((data).trim() == "error")
								  {
					                alert("Email or Password does not matched.\n Please Try Again.");
								  }	
								  else
								  {
								    window.location.href="?request=sell_page";

								  }
							}
						});
				}
	      } 
 }
 function adSubmission(){
 	var ad_title = $("#ad-title").val();
 	var product_type = $('#category-type');
 	//var product_brand = $('#category-type');
 	var product_description = $('#product-description');
 	var product_price = $('#product-price');
 	var photo_one = $('#product-photo-one');
 	var photo_two = $('#product-photo-two');
 	var photo_three = $('#product-photo-three');
 	var photo_four = $('#product-photo-four');


 	if (!ad_title || !product_type || !product_description|| !product_price) {
 		alert("All Fileds are Necessary including product photos");
 	}
 	else{
 		if(!photo_one || !photo_two || !photo_three|| !photo_four){
 			alert("Atleast one product photo is required");
 		}

 	    else{
 	    	   $.ajax({
			   method: "POST",
			   url: "",
			   data: { user_email: email, user_password: password,request:'user_login' }
			   })
			   .done(function( data, textStatus, jqXHR ) {

			    });

 		}

 	}
 }
