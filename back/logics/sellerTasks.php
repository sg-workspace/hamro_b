<?php
  include '../db/dbConnect.php';
  include '../models/onSaleItem.php';
  include '../models/user.php';
  include '../models/purchase.php';
  class SellerTasks
  {
    //note: the use of term var for properties (member variables) are
    //depracated since v5 but still backward compatible.
    //v7 and later supports data-type declaration (eg int $i)
    private $seller; //object of class User
    private $onSaleItem; //object of class OnSaleItem
    private $buyer; //object of User
    private $purchase; //object of Purchase
    function __construct($seller,$onSaleItem,$buyer,$purchase)
    {
      $this->seller = $seller;
      $this->onSaleItem = $onSaleItem;
      $this->buyer = $buyer;
      $this->purchase = $purchase;
    }

    //add item to sales list
    function addToSales(){

    }

    //remove from sales
    function removeFromSales($itemId){

    }

    //getters and setters
    public function getSeller()
    {
      return $this->seller;
    }
    public function setSeller($seller)
    {
      $this->seller = $seller;
    }
    public function getOnSaleItem()
    {
      return $this->onSaleItem;
    }
    public function setOnSaleItem($onSaleItem)
    {
      $this->onSaleItem = $onSaleItem;
    }
    public function getBuyer()
    {
      return $this->buyer;
    }
    public function setBuyer($buyer)
    {
      $this->buyer = $buyer;
    }
    public function getPurchase()
    {
      return $this->purchase;
    }
    public function setPurchase($purchase)
    {
      $this->purchase = $purchase;
    }
}

?>
