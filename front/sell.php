<?php
	include('user-header.php');
?>

<!DOCTYPE html>
	<html>
		<head>
			<title>Sell</title>
		</head>
		<body>
			<div class="selling-items-container cold-md-12">
				<div class="selling-item-lists cold-md-4 pull-left">
					<ul class="item-menu">
							<label>Mobile</label>
							<label>Motorcycle</label>
							<label>Scooter</label>
							<label>Car/Jeep</label>
							<label>Laptop</label>
							<label>Computer</label>		

					</ul>

				</div>
				<div class="selling-item-forms cold-md-8 pull-left">
					<div class="mobile-selling-form">
						<h1> Fill the details of your product</h1>
						<input type="hidden" id="itemType" itemType="none">
						<form>
							<label>Ad Title</label><br>
							<input type="text" name="" class="form-text-box" id="ad-title"><br>
							<label>Category</label><br>
							<select class="form-text-box" id="category-type">
								<option>--- Choose Brand ---</option>
								<option>Mobile</option>
								<option>Motorcycle</option>
								<option>Car</option>
								<option>Scooter</option>

							</select><br>
							<label>Brand</label><br>
							<select class="form-text-box" id="">
								<option>--- Choose Brand ---</option>
								<option>Samsung</option>
							</select><br>
							<label>Description</label><br>
							<textarea rows="6" cols="56" name="product-description" class="product-description" id="product-description"></textarea><br>
							<label>Price(NRS)</label><br>
							<input type="text" class="form-text-box" id="product-price"><br>
							<label>Price Negotiable?</label><br>
						    <input type="radio" id="mobilePriceNegYes" name="priceNegotiable" value="yes">
  							<label for="yes">Yes</label>
						    <input type="radio" id="mobilePriceNegNo" name="priceNegotiable" value="no">
  							<label for="no">Fixed Price</label><br>


  							<label>Upload Photo(4 Photos)</label><br>
  							<table class="items-photo-table">
  								<tr>
  									<td>
  										<div class="photo-container" id="product-photo-one">
											<i class="fa fa-camera"></i> 
										</div>
  									</td>
  									<td>
										<div class="photo-container" id="product-photo-two">
											<i class="fa fa-camera"></i> 
										</div>
  									</td>
  								</tr>
  								<tr>
  									<td>
										<div class="photo-container" id="product-photo-three">
											<i class="fa fa-camera"></i> 
										</div>
  									</td>
  									<td>
										<div class="photo-container" id="product-photo-four">
											<i class="fa fa-camera"></i> 
										</div>
  									</td>
  								</tr>
  							</table><br><br>
  							<input type="button" id="adSubmit" value="Submit" onclick="adSubmission();">
						</form>
					</div>
					
				</div>
			</div>
	    	<script type="text/javascript" src= "./front/js/link.js"></script>
			<script type="text/javascript" src= "./front/js/jQuery.js"></script>
		</body>
	</html>