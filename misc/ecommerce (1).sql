-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2020 at 08:58 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment_types`
--

CREATE TABLE `comment_types` (
  `type_id` varchar(15) NOT NULL,
  `type_description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

CREATE TABLE `item_category` (
  `category_id` varchar(30) NOT NULL,
  `description` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_comments`
--

CREATE TABLE `item_comments` (
  `comment_id` varchar(30) NOT NULL,
  `datetime` int(11) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `item_id` varchar(30) NOT NULL,
  `comment_type_id` varchar(15) NOT NULL,
  `commenter` varchar(30) NOT NULL,
  `reply_to` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `on_sale_items`
--

CREATE TABLE `on_sale_items` (
  `item_id` varchar(30) NOT NULL,
  `item_name` varchar(50) NOT NULL,
  `item_images_json` varchar(200) NOT NULL,
  `price` int(11) NOT NULL,
  `item_condition` int(11) NOT NULL,
  `sold_status` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `seller` varchar(30) NOT NULL,
  `category_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchase_id` varchar(30) NOT NULL,
  `date` int(11) NOT NULL,
  `buyer_id` varchar(30) NOT NULL,
  `item_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seller_feedback`
--

CREATE TABLE `seller_feedback` (
  `satisfaction_score` int(11) NOT NULL,
  `buyer_comment` int(11) NOT NULL,
  `seller_id` varchar(30) NOT NULL,
  `item_id` varchar(30) NOT NULL,
  `purchase_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userreg`
--

CREATE TABLE `userreg` (
  `id` int(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `address_district` varchar(20) NOT NULL,
  `address_area` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userreg`
--

INSERT INTO `userreg` (`id`, `first_name`, `last_name`, `email`, `password`, `mobile_no`, `address_district`, `address_area`) VALUES
(117659, 'Sajan', 'Pyatha', 'admin', 'sajan', '9860590345', 'Bhaktapur', 'Thimi'),
(152067, 'Gyurme', 'Sherpa', 'gyurme@gmail.com', 'gyurme', '8749824799', 'Boudha', 'Chahbil'),
(304650, 'Sandis', 'Prajapati', 'sajandis.prajapati@gmail.com', 'sandis', '9898989898', 'Kathmandu', 'Bansbari');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(30) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `primary_email` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `contact_no` varchar(10) NOT NULL,
  `address` varchar(256) NOT NULL,
  `varification_doc_count` int(11) DEFAULT NULL,
  `varification_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_varification`
--

CREATE TABLE `user_varification` (
  `document_name` int(11) NOT NULL,
  `document_image_blob` int(11) NOT NULL,
  `user_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment_types`
--
ALTER TABLE `comment_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `item_category`
--
ALTER TABLE `item_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `item_comments`
--
ALTER TABLE `item_comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `comment_type_id` (`comment_type_id`),
  ADD KEY `commenter` (`commenter`),
  ADD KEY `reply_to` (`reply_to`);

--
-- Indexes for table `on_sale_items`
--
ALTER TABLE `on_sale_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `seller` (`seller`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `seller_feedback`
--
ALTER TABLE `seller_feedback`
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `purchase_id` (`purchase_id`);

--
-- Indexes for table `userreg`
--
ALTER TABLE `userreg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `primary_email` (`primary_email`);

--
-- Indexes for table `user_varification`
--
ALTER TABLE `user_varification`
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `userreg`
--
ALTER TABLE `userreg`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=304651;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item_comments`
--
ALTER TABLE `item_comments`
  ADD CONSTRAINT `item_comments_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `item_comments_ibfk_2` FOREIGN KEY (`comment_type_id`) REFERENCES `comment_types` (`type_id`),
  ADD CONSTRAINT `item_comments_ibfk_3` FOREIGN KEY (`commenter`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `item_comments_ibfk_4` FOREIGN KEY (`reply_to`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `on_sale_items`
--
ALTER TABLE `on_sale_items`
  ADD CONSTRAINT `on_sale_items_ibfk_1` FOREIGN KEY (`seller`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `on_sale_items_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `item_category` (`category_id`);

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `purchases_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`);

--
-- Constraints for table `seller_feedback`
--
ALTER TABLE `seller_feedback`
  ADD CONSTRAINT `seller_feedback_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `seller_feedback_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `seller_feedback_ibfk_3` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`purchase_id`);

--
-- Constraints for table `user_varification`
--
ALTER TABLE `user_varification`
  ADD CONSTRAINT `user_varification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
