<?php
	$fileroot = "./";
	include('header.php');
?>
<!DOCTYPE html>
	<html>
		<head>
			  <link rel="stylesheet" type="text/css" href="./front/css/front.css">
			<title>Regsiter</title>
		</head>
		<body>
			<div class="container" id="container">
				<div class="form-container sign-up-container">
					<form method="POST">
						<h1>Create Account</h1>
							<span>or use your email for registration</span>

							<input type="text" id="user_first_name" placeholder="First Name" />
							<input type="text" id="user_last_name" placeholder="Last Name" />
							<input type="email"id="user_email" placeholder="Email" />
							<input type="password" id="user_password" placeholder="Password" />
							<input type="password" id="user_confirm_password" placeholder="Confirm Password" />
							<input type="number" id="user_contact" placeholder="Contact Number" />
							<input type="text" id="user_address" placeholder="Address" />
							<input type="button" onclick="userRegister();" value="Register">
					</form>
				</div>
				<div class="form-container sign-in-container">
					<form>
						<h1>Sign in</h1>
						<span>or use your account</span>
						<input type="email" placeholder="Email" id="user_signin_email" />
						<input type="password" placeholder="Password" id="user_signin_password" />
						<a href="#">Forgot your password?</a>
						<input type="button" Value="Sign In" onclick="userLogin();">
					</form>
				</div>
				<div class="overlay-container">
					<div class="overlay">
						<div class="overlay-panel overlay-left">
							<h1>Welcome Back!</h1>
							<p>To keep connected with us please login with your personal info</p>
							<button class="ghost" id="signIn">Sign In</button>
						</div>
						<div class="overlay-panel overlay-right">
							<h1>Hello, Friend!</h1>
							<p>Enter your personal details and start journey with us</p>
							<button class="ghost" id="signUp">Sign Up</button>
						</div>
					</div>
				</div>
			</div>

		</body>
		<script type="text/javascript" src= "./front/js/link.js"></script>
		<script type="text/javascript" src= "./front/js/jQuery.js"></script>
	</html>