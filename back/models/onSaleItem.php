<?php

  class OnSaleItem
  {
    //note: the use of term var for properties (member variables) are
    //depracated since v5 but still backward compatible.
    //v7 and later supports data-type declaration (eg int $i)
    private $item_id;
    private $name;

    public function getItemId()
    {
      return $this->item_id;
    }
    public function setItemId($item_id)
    {
      $this->item_id = $item_id;
      return $this;
    }
    public function getName()
    {
      return $this->name;
    }
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }
}


?>
